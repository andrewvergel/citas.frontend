import empty from 'is-empty';
import FirebaseService  from '../../_core/_backend/_services/Firebase.service'

class AuthService {

    static logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
    }

    static getCurrentUser() {
        return JSON.parse(localStorage.getItem('user'));
    }

    static getUserProfile() {
        let user = AuthService.getCurrentUser()
        return (!empty(user)) ? user.type : '';
    }

    static async register(user) {


        try {

            let newUserAuth =  await FirebaseService.authRef.createUserWithEmailAndPassword(user.username, user.password);

            if (!empty(newUserAuth.user)) {

                let newUser = await FirebaseService.add({
                    obj: 'users',
                    uid: newUserAuth.user.uid,
                    data: user
                })

                await FirebaseService.authRef.currentUser.sendEmailVerification();

                return newUser;

            }else{
                throw new Error('Error: no se logro registrar el usuario correctamente');
            }
            
            
        } catch (error) {

            console.log('Error::', error);
            throw new Error('Error: no se logro registrar el usuario correctamente');
            
        }


    }

    static async login({username, password} = {}) {

        try {

            let loggedUser =  await FirebaseService.authRef.signInWithEmailAndPassword(username, password);
            console.log("login:loggedUser::", loggedUser.user);

            let currentDataUser =  await FirebaseService.getById({
                obj : 'users',
                uid : loggedUser.user.uid
            })

            console.log("login:currentDataUser::", currentDataUser)

            if (!empty(currentDataUser)) {
                currentDataUser.auth = loggedUser.user;
                localStorage.setItem('user', JSON.stringify(currentDataUser));
                return currentDataUser;
            }else{
                throw new Error('Usuario / Contraseña son invalidos');
            }
            
        } catch (error) {

            console.log('Error::', error);
            throw new Error('Usuario / Contraseña son invalidos');
            
        }
        
    }

}

export default AuthService;