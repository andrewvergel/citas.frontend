import ApiService from '../../_core/_backend/_services/Api.service'

class UserService {

    static getAll(params = {}) {

        return ApiService.handlerFetchGet({
            url: '/users',
            params
        });

    }

}

export default UserService;