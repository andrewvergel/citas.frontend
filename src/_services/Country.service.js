import ApiService  from '../../_core/_backend/_services/Api.service'
import FirebaseService  from '../../_core/_backend/_services/Firebase.service'

class CountryService {

    static async getAll(params = {}) {


        try {

            return await FirebaseService.getAll({
                obj: 'countries'
            })

        } catch (error) {

            throw new Error('Error: no se logro registrar el pais correctamente');

        }
        
    }    

    static async getById(uid) {

        return await FirebaseService.getById({
            obj : 'countries',
            uid : uid
        })


    }

    static async add(country){

        try {

            return await FirebaseService.add({
                obj: 'countries',
                data: country
            })

        } catch (error) {

            throw new Error('Error: no se logro registrar el pais correctamente');

        }

        

    }

    static async edit(country){

        console.log('CountryService:edit:country', country);

        try {

            return await FirebaseService.update({
                uid: country.id,
                obj: 'countries',
                data: country
            })

        } catch (error) {

            throw new Error('Error: no se logro registrar el pais correctamente');

        }

    }

    static async delete(uid){

        console.log('CountryService:delete:country', uid);

        try {

            return await FirebaseService.delete({
                uid: uid,
                obj: 'countries'
            })

        } catch (error) {

            throw new Error('Error: no se logro registrar el pais correctamente');

        }

    }

}

export default CountryService;