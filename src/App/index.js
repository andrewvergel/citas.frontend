import React from 'react';
import empty from 'is-empty'
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';

import { history } from '../../_core/_helpers/history';
import AlertActions from '../_actions/Alert.actions';
import { PrivateRoute } from '../_components/PrivateRoute';
import { PrivateRouteAdmin } from '../_components/PrivateRouteAdmin';
import Grid from '@material-ui/core/Grid';
import '../_css/App.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import AlertNotification from '../../_core/_ui/_components/AlertNotification' 

import AppConfig from '../_config/app'
import Header from '../_components/Header';
import Menu from '../_components/Menu'
import Footer from '../_components/Footer';

import { HomePage } from '../_containers/HomePage';
import { LoginPage } from '../_containers/LoginPage';
import { RegisterPage } from '../_containers/RegisterPage';
import { CartPage } from '../_containers/CartPage';
import { DashboardPage } from '../_containers/DashboardPage';
import { CountriesPage } from '../_containers/CountriesPage';
import { TownsPage } from '../_containers/TownsPage';


const drawerWidth = AppConfig.header.drawerWidth;

const styles = theme => ({
    root: {
      display: 'flex',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing.unit * 3,
      [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        marginTop: "65px"
      },
    },
    toolbar: {
        [theme.breakpoints.down('sm')]: {
            marginTop: "65px"
        },
    },
});

class App extends React.Component {

    constructor(props) {
        super(props);
        const { dispatch } = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(AlertActions.clear());
        });

        this.state = {
            open: false
        }
    }
    
    handleDrawerToggle = () => {
        this.setState({ open: !this.state.open });
    };

    render() {

        const { classes, theme, alert } = this.props;

        return (
            <MuiThemeProvider theme={createMuiTheme(AppConfig.theme)}>
            
                    <Router history={history}>

                        <div>
                            
                            <Header handleDrawerToggle={this.handleDrawerToggle} />

                            <Menu {...this.state} history={history} handleDrawerToggle={this.handleDrawerToggle} />

                            <Grid item xs={12} className={classes.content}>

                            <div className={classes.toolbar} />

                            {
                                (!empty(alert.message)) ? 
                                    <AlertNotification {...alert} />
                                : ''
                            }

                            <PrivateRoute exact path="/" component={HomePage} />
                            <PrivateRoute exact path="/cart" component={CartPage} />
                            <PrivateRoute exact path="/dashboard" component={DashboardPage} />
                            <PrivateRouteAdmin exact path="/countries" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/add" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/view/:id" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/edit/:id" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/delete/:id" component={CountriesPage} />
                            <PrivateRouteAdmin path="/towns" component={TownsPage} />
                            <Route path="/login" component={LoginPage} />
                            <Route path="/register" component={RegisterPage} />

                            <Footer />

                            </Grid>

                            

                        </div>

                        

                    </Router>

                
                
            </MuiThemeProvider>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = withStyles(styles, { withTheme: true })(connect(mapStateToProps)(App));
export { connectedApp as App }; 