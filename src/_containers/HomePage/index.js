import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import '../../_css/App.css';
import { withStyles } from '@material-ui/core/styles';

import UserActions   from '../../_actions/User.actions';

import {    getAvails, 
            getCountries,
            getTowns,
            getEmployees,
            getServices,
            getEmployeesDateAvails } from '../../_helpers/fake-data';

const styles = theme => ({
    button: {
        marginRight : theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    inputText:{
        marginTop: 0,
        width: '100%'
    }
});

class HomePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          avails : getAvails(),
          availsEmployees : getEmployeesDateAvails(),
          countries : getCountries(),
          towns : getTowns(),
          employees:getEmployees(),
          services:getServices(),
          pickedCountry : '',
          pickedEmployee : '',
          pickedService : '',
          pickedTown : '',
          pickedLocality : '',
          pickedDay : '',
          pickedTime : ''
        };

         this.handleChange = this.handleChange.bind(this);
         this.handleChangeCountry = this.handleChangeCountry.bind(this);
         this.handleChangeLocality = this.handleChangeLocality.bind(this);
         this.handleChangeService = this.handleChangeService.bind(this);
      }

    buildCountries () {

        let countriesSel = [<MenuItem key={-1} value=""><em>Seleccione un país</em></MenuItem>],
            countries = this.state.countries;
     
        for (var country in countries) {
          if (countries.hasOwnProperty(country)) {
            countriesSel.push(<MenuItem key={country} value={country}>{country}</MenuItem>)
          }
        }
        
        return countriesSel;
    }

    buildTowns(pickedCountry){
        let towns = 
          [<MenuItem key={-1} value=""><em>Seleccione un estado</em></MenuItem>],
            countries = this.state.countries;
        for (var town in countries) {
          if (countries.hasOwnProperty(town) && town === pickedCountry) {
            countries[town].map( 
                (t) => towns.push(
                   <MenuItem key={t} value={t}>{t}</MenuItem>
              ))
          }
        }
        return towns;
      }
    
    buildLocalities(pickedTown){
        let localities = 
          [<MenuItem key={-1} value=""><em>Seleccione una localidad</em></MenuItem>],
          towns = this.state.towns;
        for (var locality in towns) {
          if (towns.hasOwnProperty(locality) && locality === pickedTown) {
            towns[locality].map( 
                (t) => localities.push(
                    <MenuItem key={t} value={t}>{t}</MenuItem>
              ))
          }
        }
        return localities;
      }

    buildEmployees(pickedLocality){
        let employeesSel = 
          [<MenuItem key={-1} value=""><em>Seleccione un empleado</em></MenuItem>],
          employees = this.state.employees;
        for (var employee in employees) {
          if (employees.hasOwnProperty(employee) && employee === pickedLocality) {
            employees[employee].map( 
                (t) => employeesSel.push(
                    <MenuItem key={t} value={t}>{t}</MenuItem>
              ))
          }
        }
        return employeesSel;
      }
    
    buildServices(pickedEmployee){
        let servicesSel = 
          [<MenuItem key={-1} value=""><em>Seleccione un servicio</em></MenuItem>],
          services = this.state.services;
        for (var service in services) {
          if (services.hasOwnProperty(service) && service === pickedEmployee) {
            services[service].map( 
                (t) => servicesSel.push(
                    <MenuItem key={t} value={t}>{t}</MenuItem>
              ))
          }
        }
        return servicesSel;
      }

    buildDays (pickedEmployee) {
        let days = [<MenuItem key={-1} value=""><em>Seleccione Fecha Disponible</em></MenuItem>],
            avails = this.state.availsEmployees;
     
        for (var day in avails) {
          if (avails.hasOwnProperty(day) && day === pickedEmployee) {
            avails[day].map( 
                (t) => days.push(
                    <MenuItem key={t} value={t}>{t}</MenuItem>
              ))
          }
        }
        return days;
      }
     
    buildTimes(pickedDay){
        let times = 
          [<MenuItem key={-1} value=""><em>Seleccione Hora Disponible</em></MenuItem>],
            avails = this.state.avails;
        for (var time in avails) {
          if (avails.hasOwnProperty(time) && time === pickedDay) {
              avails[time].map( 
                (t) => times.push(
                    <MenuItem key={t} value={t}>{t}</MenuItem>
              ))
          }
        }
        return times;
      }

    

    componentDidMount() {
        this.props.dispatch(UserActions.getAll());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(UserActions.delete(id));
    }

    handleChange(event) {
        let name  = event.target.name,
            val = event.target.value;
     
        name === 'dates' ? 
           this.setState({pickedDay: val}) : 
           this.setState({pickedTime: val})
        setTimeout( () => console.log(this.state), 200)
      }
    
    handleChangeCountry(event) {

        let name  = event.target.name,
            val = event.target.value;
     
        name === 'countries' ? 
           this.setState({pickedCountry: val,pickedTown:''}) : 
           this.setState({pickedTown: val,pickedLocality:''})
        setTimeout( () => console.log(this.state), 200)
      }

      handleChangeLocality(event) {
        let name  = event.target.name,
            val = event.target.value;
     
        name === 'localities' ? 
           this.setState({pickedLocality: val,pickedEmployee:''}) : 
           this.setState({pickedEmployee: val,pickedService:''})
        setTimeout( () => console.log(this.state), 200)
      }

      handleChangeService(event) {
        let name  = event.target.name,
            val = event.target.value;
     
        name === 'services' ? 
           this.setState({pickedService: val}) : 
           ''
        setTimeout( () => console.log(this.state), 200)
      }
     
      timesInput() {
        if (this.state.pickedDay.length) {
          let input = <Grid item xs={12}>

                        <FormControl className="FormControlSelect">
                                <InputLabel htmlFor="times">Hora Disponible:</InputLabel>
                                <Select
                                    value={this.state.pickedTime}
                                    onChange={this.handleChange}
                                    name="times"
                                >
                                {this.buildTimes(this.state.pickedDay)}
                                </Select>
                        </FormControl>

                        {this.servicesInput()}
                      </Grid>;
          return input;
        }
      }

    townsInput() {

        if (this.state.pickedCountry.length) {

            let input = <Grid item xs={12}>
                            <FormControl className="FormControlSelect">
                                <InputLabel htmlFor="towns">Estados</InputLabel>
                                <Select
                                    value={this.state.pickedTown}
                                    onChange={this.handleChangeCountry}
                                    name="towns"
                                >
                                {this.buildTowns(this.state.pickedCountry)}
                                </Select>
                            </FormControl>
                            {this.localitiesInput()}
                        </Grid>
                        

            return input;
        }

      }

    localitiesInput() {
        if (this.state.pickedTown.length) {
          let input = <Grid item xs={12}>
                        
                        <FormControl className="FormControlSelect">
                                <InputLabel htmlFor="localities">Localidades</InputLabel>
                                <Select
                                    value={this.state.pickedLocality}
                                    onChange={this.handleChangeLocality}
                                    name="localities"
                                >
                                {this.buildLocalities(this.state.pickedTown)}
                                </Select>
                        </FormControl>
                        
                        {this.employeesInput()}
                      </Grid>;
          return input;
        }
    }

    employeesInput() {
        if (this.state.pickedLocality.length) {
          let input = <Grid item xs={12}>
                        <FormControl className="FormControlSelect">
                                <InputLabel htmlFor="employees">Personal</InputLabel>
                                <Select
                                    value={this.state.pickedEmployee}
                                    onChange={this.handleChangeLocality}
                                    name="employees"
                                >
                                {this.buildEmployees(this.state.pickedLocality)}
                                </Select>
                        </FormControl>
                        {this.datesInput()}
                      </Grid>;
          return input;
        }
    }


    servicesInput() {
        if (this.state.pickedEmployee.length) {
          let input = <Grid item xs={12}>

                        <FormControl className="FormControlSelect">
                                <InputLabel htmlFor="services">Servicios</InputLabel>
                                <Select
                                    value={this.state.pickedService}
                                    onChange={this.handleChangeService}
                                    name="services"
                                >
                                {this.buildServices(this.state.pickedEmployee)}
                                </Select>
                        </FormControl>

                      </Grid>
          return input;
        }
    }

    datesInput() {
        if (this.state.pickedEmployee.length) {
          let input = <Grid item xs={12}>

                        <FormControl className="FormControlSelect">
                                <InputLabel htmlFor="dates">Fecha Disponible</InputLabel>
                                <Select
                                    value={this.state.pickedDay}
                                    onChange={this.handleChange}
                                    name="dates"
                                >
                                {this.buildDays(this.state.pickedEmployee)}
                                </Select>
                        </FormControl>

                        {this.timesInput()}
                      </Grid>;
          return input;
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        console.log("Submit Citas");
    }

    render() {
        const { user, users, classes } = this.props;
        return (
            <Grid item xs={12} sm={6} className="ContentApp">
                <Paper>

                    <Typography component="p" color="textPrimary" className="FormHeader">
                        Bienvenid@ {user.firstName} {user.lastName}
                    </Typography>

                    <form name="formCitas" onSubmit={this.handleSubmit}>
                    
                        <Grid item xs={12}>

                            <Grid item xs={12}>
                                <FormControl className="FormControlSelect">
                                    <InputLabel htmlFor="countries">País</InputLabel>
                                    <Select
                                        value={this.state.pickedCountry}
                                        onChange={this.handleChangeCountry}
                                        name="countries"
                                    >
                                    {this.buildCountries()}
                                    </Select>
                                </FormControl>
                            </Grid>

                            {this.townsInput()} 

                            
                        </Grid>
                        <Grid item xs={12} className="FormFooter">
                            <Button className={classes.button} component={Link} to="/cart" type='submit' variant='contained' color='primary'>Solicitar Cita</Button>
                            <Button className={classes.button} type='submit' variant='contained' color='secondary'>Cancelar</Button>
                        </Grid>
                    </form>
                </Paper>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = withStyles(styles)(connect(mapStateToProps)(HomePage));
export { connectedHomePage as HomePage };