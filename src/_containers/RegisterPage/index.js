/**
 * Pagina para registrar usuarios
 * autor: Andres Rosales
 * fecha: 29/12/2018
 */

 // Librerias 
import React from 'react';
import { connect } from 'react-redux';
import empty from 'is-empty'
import Validator from 'validator'

// Componentes UI
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import DropDown from '../../../_core/_ui/_components/DropDown';
import '../../_css/App.css';

// Componentes dinamicos
import FormsUtils from '../../../_core/_ui/_utils/FormsUtils'
import { getTypeUsers } from '../../_helpers/fake-data';
import AuthActions from '../../_actions/Auth.actions';


class RegisterPage extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            user: {
                firstName: '',
                lastName: '',
                username: '',
                type: '',
                password: '',
                phone: '',
                birthday:''
            },
            touched: {
                firstName: false,
                lastName: false,
                type: false,
                username: false,
                password: false,
                phone: false,
                birthday:false
            },
            activedButtonSubmit: false,
            errors: {},
            submitted: false
        };

    }

    validate(){
        
        const   { 
                    user, 
                    touched 
                } = this.state;

        const   {   
                    firstName,
                    lastName,
                    type,
                    birthday,
                    phone,
                    username,
                    password,
                } = user;

        let errors = {}

        if(touched.firstName){
            if(Validator.isEmpty(firstName)) errors.firstName = 'Nombres es un campo obligatorio'
        }

        if(touched.lastName){
            if(Validator.isEmpty(lastName)) errors.lastName = 'Apellidos es un campo obligatorio'
        }

        if(touched.type){
            let typeString = (empty(type)) ? '' : type.toString();
            if(Validator.isEmpty(typeString)) errors.type = 'Tipo de usuario es un campo obligatorio'
        }

        if(touched.birthday){
            if(Validator.isEmpty(birthday)) errors.birthday = 'Fecha de nacimiento es un campo obligatorio'
        }
        
        if(touched.phone){
            if(Validator.isEmpty(phone)) errors.phone = 'Telefono es un campo obligatorio'
        }

        if(touched.username){

            if(!Validator.isEmail(username)) errors.username = 'Correo electronico debe ser de tipo correo'
            if(Validator.isEmpty(username)) errors.username = 'Correo electronico es un campo obligatorio'
            
        }

        if(touched.password){
            if(Validator.isEmpty(password)) errors.password = 'Contraseña es un campo obligatorio'
        }
        
        this.setState({
            errors
        }, () => {
            FormsUtils.shouldBeSubmit(this)
        })

        return (empty(errors)) ? true : false;
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        dispatch(AuthActions.register(user));
    }

    render() {

        const { user, 
                submitted, 
                errors, 
                activedButtonSubmit } = this.state;

        return (

            <Grid   item 
                    xs={12} 
                    sm={6} 
                    className="ContentApp">
                
                <Paper>

                    <Typography variant="h5" 
                                component="h3" 
                                color="textPrimary" 
                                className="FormHeader" >
                            Registro
                    </Typography>

                    <form   name="form" 
                            onSubmit={(e) => this.handleSubmit(e)} >

                        <Grid   item 
                                xs={12} >
                            <TextField  {...((errors.firstName) && { error: true })}
                                        helperText={(errors.firstName) ? errors.firstName : '' }
                                        id="registerFirstName"
                                        label="Nombre"
                                        name='firstName'
                                        className="FormInputText"
                                        value={user.firstName}
                                        onChange={(e) => FormsUtils.handleChange({ e, context: this, target: 'user' })}
                                        onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                        margin="normal"
                            />
                        </Grid>

                        <Grid   item 
                                xs={12} >
                            <TextField  {...((errors.lastName) && { error: true })}
                                        helperText={(errors.lastName) ? errors.lastName : '' }
                                        id="registerLastName"
                                        label="Apellido"
                                        name='lastName'
                                        className="FormInputText"
                                        value={user.lastName}
                                        onChange={(e) => FormsUtils.handleChange({ e, context: this, target: 'user' })}
                                        onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                        margin="normal"
                                />
                        </Grid>

                        <DropDown   {...((!empty(errors.type)) && { error: true })}
                                    helperText={(!empty(errors.type)) ? errors.type : '' }
                                    data={ getTypeUsers() } 
                                    keyD="id" value="description" 
                                    label="Tipos de usuario" 
                                    name="type"  
                                    handleChange={(e) => FormsUtils.handleChange({ e, context: this, target: 'user' })}
                                    onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                    selected={this.state.user.type} 
                        />

                        <Grid   item 
                                xs={12} >
                            <TextField  {...((errors.birthday) && { error: true })}
                                        helperText={(errors.birthday) ? errors.birthday : '' }
                                        id="registerBirthday"
                                        label="Fecha de Nacimiento"
                                        name='birthday'
                                        className="FormInputText"
                                        value={user.birthday}
                                        onChange={(e) => FormsUtils.handleChange({ e, context: this, target: 'user' })}
                                        onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                        margin="normal"
                            />
                        </Grid>

                        <Grid   item 
                                xs={12} >
                            <TextField  {...((errors.phone) && { error: true })}
                                        helperText={(errors.phone) ? errors.phone : '' }
                                        id="registerPhone"
                                        label="Telefono"
                                        name='phone'
                                        className="FormInputText"
                                        value={user.phone}
                                        onChange={(e) => FormsUtils.handleChange({ e, context: this, target: 'user' })}
                                        onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                        margin="normal"
                            />
                        </Grid>

                        <Grid   item 
                                xs={12} >
                            <TextField  
                                        {...((errors.username) && { error: true })}
                                        helperText={(errors.username) ? errors.username : '' }
                                        id="registerUsername"
                                        label="Correo Electronico"
                                        name='username'
                                        className="FormInputText"
                                        value={user.username}
                                        onChange={(e) => FormsUtils.handleChange({ e, context: this, target: 'user' })}
                                        onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                        margin="normal"
                            />
                        </Grid>

                        <Grid   item 
                                xs={12} >
                            <TextField  
                                        {...((errors.password) && { error: true })}
                                        helperText={(errors.password) ? errors.password : '' }
                                        id="registerPassword"
                                        label="Contraseña"
                                        name='password'
                                        type='password'
                                        className="FormInputText"
                                        value={user.password}
                                        onChange={(e) => FormsUtils.handleChange({ e, context: this, target: 'user' })}
                                        onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                        margin="normal"
                            />
                        </Grid>

                        <Grid   item 
                                xs={12} 
                                className="FormFooter" >
                            <Button className="FormButton" 
                                    type="submit" 
                                    variant="contained" 
                                    color="primary" 
                                    disabled={!activedButtonSubmit} >
                                    Registro
                            </Button>
                            
                            <Button className="FormButton" 
                                    onClick={() => {  this.props.history.push(`/login`)}} 
                                    variant="contained" 
                                    color="secondary">Cancelar</Button>
                        </Grid>

                    </form>

                </Paper>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage };