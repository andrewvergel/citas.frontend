import React from 'react';
import { shallow, mount, render } from 'enzyme';
import configureStore from 'redux-mock-store'

// Mock Store
const initialState = {
    authentication : {
        loggingIn : false
    }
}; 
const mockStore = configureStore();

// Container to Test
import { LoginPage } from './index';

describe('Login Page Suite', function() {

    it('debe cargar correctamente el titulo', function() {
        
        const store = mockStore(initialState)
        const wrapper = mount(
            <LoginPage store={store} />
          );
        const h3 = wrapper.find('h3');
        expect(h3.text()).toBe('Solicitud de Citas');
        
    });

})