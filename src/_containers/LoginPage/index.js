/**
 * Pagina de Login
 * autor: Andres Rosales
 * fecha: 20/12/2018
 */
import React from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import  '../../_css/App.css';

import empty from 'is-empty'
import Validator from 'validator'
import AuthActions from '../../_actions/Auth.actions';
import FormsUtils from '../../../_core/_ui/_utils/FormsUtils'


class LoginPage extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(AuthActions.logout());

        this.state = {
            user: {
                username: '',
                password: '',
            },
            touched: {
                username: false,
                password: false,
            },
            activedButtonSubmit: false,
            errors: {},
            submitted: false
        };

    }

    validate(){
        
        const { user, touched } = this.state;
        let { username, password } = user;
        let errors = {}
        
        if(touched.username){
            if(Validator.isEmpty(username)) errors.username = 'Usuario es un campo obligatorio'
        }

        if(touched.password){
            if(Validator.isEmpty(password)) errors.password = 'Contraseña es un campo obligatorio'
        }
        
        this.setState({
            errors
        }, () => {
            FormsUtils.shouldBeSubmit(this)
        })

        return (empty(errors)) ? true : false;
    }

    handleSubmit(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        const { username, 
                password } = this.state.user;
        this.setState({ submitted: true });
        dispatch(AuthActions.login({username, password}));
    }

    render() {

        const { user, 
                errors, 
                activedButtonSubmit } = this.state;
        let {   username, 
                password } = user;

        return (
            <Grid   item 
                    xs={12} 
                    sm={6} 
                    className="ContentApp" >
                    
                <Paper>

                    <Typography variant="h5" 
                                component="h3" 
                                color="textPrimary" 
                                className="FormHeader text-center" >
                        Solicitud de Citas
                    </Typography>

                    <form   name="form" 
                            onSubmit={(e) => this.handleSubmit(e)} >

                    <Grid   container 
                            spacing={8} >

                        <Grid   item 
                                xs={12} >

                            <TextField
                                    {...((errors.username) && { error: true })}
                                    helperText={(errors.username) ? errors.username : '' } 
                                    id="loginUsername"
                                    label="Usuario"
                                    name='username'
                                    className="FormInputText"
                                    value={username}
                                    onChange={(e) => FormsUtils.handleChange({e, context: this, target: 'user' })}
                                    onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                    margin="normal"
                                />
                        </Grid>


                        <Grid   item 
                                xs={12} >
                            <TextField
                                    {...((errors.password) && { error: true })}
                                    helperText={(errors.password) ? errors.password : '' } 
                                    id="loginPassword"
                                    label="Contraseña"
                                    type="password"
                                    name='password'
                                    className="FormInputText"
                                    value={password}
                                    onChange={(e) => FormsUtils.handleChange({e, context: this, target: 'user'})}
                                    onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                    margin="normal"
                                />
                        </Grid>

                        <Grid   item 
                                xs={12} 
                                className="FormFooter" >

                            <Button className="FormButton" 
                                    type="submit" 
                                    variant="contained" 
                                    color="primary" 
                                    disabled={!activedButtonSubmit} >
                                    Inicio
                            </Button>

                            <Button className="FormButton" 
                                    onClick={() => { this.props.history.push(`/register`) }} 
                                    variant="contained" 
                                    color="secondary" >
                                    Registro
                            </Button>

                        </Grid>

                    </Grid>
                        
                    </form>
                    
                    
                </Paper>
            </Grid>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 