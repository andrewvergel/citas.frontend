import React from 'react';
import { connect } from 'react-redux';

// Core
import Utils from '../../../_core/_ui/_utils/Utils';

//Components
import ContentApp from '../../_components/ContentApp'
import { CountriesList } from '../../_components/Countries/CountriesList';
import CountryAdd from '../../_components/Countries/CountryAdd';
import CountryEdit from '../../_components/Countries/CountryEdit';

import '../../_css/App.css';

class CountriesPage extends React.Component {

    constructor(props) {
        super(props);
    }

    urls = {
        "^/countries" : CountriesList,
        "^/country/add" : CountryAdd,
        "^/country/edit/.*" : CountryEdit
    }
    
    render() {

        let TagName = Utils.getComponentByUrlList({
            urls        : this.urls,
            uri         : this.props.match.url
        })
        
        return (
            <ContentApp {...this.props} component={TagName} />   
        );

    }
}


function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user
    };
}

const connectedCountriesPage = connect(mapStateToProps)(CountriesPage);
export { connectedCountriesPage as CountriesPage };