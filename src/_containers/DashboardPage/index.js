import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import '../../_css/App.css';

import { getEvents } from '../../_helpers/fake-data';

import BigCalendar from 'react-big-calendar'
import moment from 'moment'

import '../../_css/react-big-calendar.css'

const localizer = BigCalendar.momentLocalizer(moment) 

const styles = theme => ({
    button: {
        marginRight : theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    inputText:{
        marginTop: 0,
        width: '100%'
    }
});

let allViews = Object.keys(BigCalendar.Views).map(k => BigCalendar.Views[k]);

class DashboardPage extends React.Component {

    constructor(props) {
        super(props);

        // reset login status
        
    }


    render() {

        let myEventsList = getEvents();
        return (
            <Grid item xs={12} sm={8} className="ContentApp">
                <Paper>

                    <Typography variant="h5" component="h3" color="textPrimary" className="FormHeader">
                        Citas confirmadas de Spa Manos Centro Plaza
                    </Typography>

                    <BigCalendar
                    className="ContentCalendar"
                    localizer={localizer}
                    events={myEventsList}
                    startAccessor="start"
                    endAccessor="end"
                    />
                </Paper>
                
            </Grid>
            
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedDashboardPage = withStyles(styles)(connect(mapStateToProps)(DashboardPage));
export { connectedDashboardPage as DashboardPage }; 