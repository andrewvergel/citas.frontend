import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import '../../_css/App.css';
import { getTowns } from '../../_helpers';
import { getCountries } from '../../_helpers';

import { userActions } from '../../_actions';

import { getPickedData } from '../../_helpers';

const TAX_RATE = 0.16;

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  button: {
    marginRight : theme.spacing.unit,
  },
  table: {
    minWidth: 700,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});


class OfficesPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          //pickedData : getPickedData(),
          methodOfPayment : '',
          open: false,
          openTrans: false,
          scroll: 'paper',
          spacing: '16',
          countries : getCountries(),
          towns : getTowns(),
          pickedTown : '',
        };

        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
      }

    handleClickOpen() {
    this.setState({ open: true});
    };

    handleClose() {
    this.setState({ open: false });
    };

    buildTowns () {
        let townsTable = [],
        towns = this.state.towns;
        console.log('towns',towns);
        for (var office in towns) {
          if (towns.hasOwnProperty(office)) {
            towns[office].map( 
                (t) => townsTable.push(<TableRow key={t}>
                                <TableCell>{office}</TableCell>
                                <TableCell>{t}</TableCell>
                                <TableCell>Activo</TableCell>
                               </TableRow>))
          }
        }
        
        return townsTable;
    }

    handleChange(event) {

        let name  = event.target.name,
            val = event.target.value;
     
        name === 'towns' ? 
           this.setState({pickedTown: val}) : 
           this.setState({pickedTown: val})
        setTimeout( () => console.log(this.state), 200)
      }

    buildTownsSel () {
        let towns = 
        [<MenuItem key={-1} value=""><em>Seleccione un estado</em></MenuItem>],
          countries = this.state.countries;
      for (var town in countries) {
        if (countries.hasOwnProperty(town)) {
          countries[town].map( 
              (t) => towns.push(
                 <MenuItem key={t} value={t}>{t}</MenuItem>
            ))
        }
      }
      return towns;
    }
    
    render() {
        const { user, users, classes  } = this.props;
        const { spacing } = this.state;
        return (
            <Grid item xs={12} sm={8} className="ContentApp">
            <Paper className={classes.root}>
            <Typography component="p" color="textPrimary" className="FormHeader">
                        Lista de Franquicias
            </Typography>
                    <Table className={classes.table}>
                        <TableHead>
                        <TableRow>
                            <TableCell>Estado</TableCell>
                            <TableCell>Franquicia</TableCell>
                            <TableCell>Estatus</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {this.buildTowns()}
                        </TableBody>
                    </Table>
                    
            </Paper>
                    <Grid container item xs={12} className="FormFooter" spacing={Number(spacing)}>
                        <Button className={classes.button} onClick={this.handleClickOpen} variant='contained' color='primary'>Agregar Franquicia</Button>
                        <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby="form-dialog-title"
                        >
                        <DialogTitle id="form-dialog-title">Agregar Franquicia</DialogTitle>
                        <DialogContent>
                            <InputLabel htmlFor="towns">Estado</InputLabel>
                            <Select
                                name="towns"
                                value={this.state.pickedTown}
                                onChange={this.handleChange}
                            >
                            {this.buildTownsSel()}
                            </Select>
                            <TextField
                            autoFocus
                            margin="dense"
                            id="office"
                            label="Franquicia"
                            fullWidth
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Agregar
                            </Button>
                            <Button onClick={this.handleClose} color="secondary">
                            Cancelar
                            </Button>
                        </DialogActions>
                        </Dialog>
                    </Grid>
            </Grid>      
          
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedOfficesPage = withStyles(styles)(connect(mapStateToProps)(OfficesPage));
export { connectedOfficesPage as OfficesPage };