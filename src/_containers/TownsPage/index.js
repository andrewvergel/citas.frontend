import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import '../../_css/App.css';
import { getCountries } from '../../_helpers/fake-data';

const TAX_RATE = 0.16;

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  button: {
    marginRight : theme.spacing.unit,
  },
  table: {
    minWidth: 700,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});


class TownsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          //pickedData : getPickedData(),
          methodOfPayment : '',
          open: false,
          openTrans: false,
          scroll: 'paper',
          spacing: '16',
          countries : getCountries(),
          pickedCountry : '',
        };

        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChangeCountry = this.handleChangeCountry.bind(this);
      }

    handleClickOpen() {
    this.setState({ open: true});
    };

    handleClose() {
    this.setState({ open: false });
    };

    buildCountries () {
        let countriesTable = [],
        countries = this.state.countries;
        console.log('countries',countries);
        for (var town in countries) {
          if (countries.hasOwnProperty(town)) {
            countries[town].map( 
                (t) => countriesTable.push(<TableRow key={t}>
                                <TableCell>{town}</TableCell>
                                <TableCell>{t}</TableCell>
                                <TableCell>Activo</TableCell>
                               </TableRow>))
          }
        }
        
        return countriesTable;
    }

    handleChangeCountry(event) {

        let name  = event.target.name,
            val = event.target.value;
     
        name === 'countries' ? 
           this.setState({pickedCountry: val}) : 
           this.setState({pickedCountry: val})
        setTimeout( () => console.log(this.state), 200)
      }

    buildCountriesSel () {

        let countriesSel = [<MenuItem key={-1} value=""><em>Seleccione un país</em></MenuItem>],
            countries = this.state.countries;
     
        for (var country in countries) {
          if (countries.hasOwnProperty(country)) {
            countriesSel.push(<MenuItem key={country} value={country}>{country}</MenuItem>)
          }
        }
        
        return countriesSel;
    }
    
    render() {
        const { user, users, classes  } = this.props;
        const { spacing } = this.state;
        return (
            <Grid item xs={12} sm={8} className="ContentApp">
            <Paper className={classes.root}>
            <Typography component="p" color="textPrimary" className="FormHeader">
                        Lista de Estados
            </Typography>
                    <Table className={classes.table}>
                        <TableHead>
                        <TableRow>
                            <TableCell>Pais</TableCell>
                            <TableCell>Estado</TableCell>
                            <TableCell>Estatus</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {this.buildCountries()}
                        </TableBody>
                    </Table>
                    
            </Paper>
                    <Grid container item xs={12} className="FormFooter" spacing={Number(spacing)}>
                        <Button className={classes.button} onClick={this.handleClickOpen} variant='contained' color='primary'>Agregar Estado</Button>
                        <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby="form-dialog-title"
                        >
                        <DialogTitle id="form-dialog-title">Agregar Estado</DialogTitle>
                        <DialogContent>
                            <InputLabel htmlFor="countries">País</InputLabel>
                            <Select
                                name="countries"
                                value={this.state.pickedCountry}
                                onChange={this.handleChangeCountry}
                            >
                            {this.buildCountriesSel()}
                            </Select>
                            <TextField
                            autoFocus
                            margin="dense"
                            id="country"
                            label="Estado"
                            fullWidth
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Agregar
                            </Button>
                            <Button onClick={this.handleClose} color="secondary">
                            Cancelar
                            </Button>
                        </DialogActions>
                        </Dialog>
                    </Grid>
            </Grid>      
          
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedTownsPage = withStyles(styles)(connect(mapStateToProps)(TownsPage));
export { connectedTownsPage as TownsPage };