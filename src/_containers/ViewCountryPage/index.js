import React from 'react';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TableList from '../../../_core/_ui/_components/TableList';
import '../../_css/App.css';
import { getCountries } from '../../_helpers';


const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    button: {
      marginRight : theme.spacing.unit,
    },
    table: {
      minWidth: 700,
    },
    control: {
      padding: theme.spacing.unit * 2,
    },
  });


class ViewCountryPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            scroll: 'paper',
            spacing: '16',
          };

      }
    
    render() {
        const { classes  } = this.props;
        const { spacing } = this.state;
        let headers = ['Id','País']; // send headers
        let keys = ['id','name']; // send key
        let actions = ['D','M','V']; //D: Delete, M: Modify, V: View
        let primaryKey = 'id'; //Primary key use for delete, modify and view

        return (
            <Grid item xs={12} sm={8} className="ContentApp">
            <Paper className={classes.root}>
            <Typography component="p" color="textPrimary" className="FormHeader">
                        Lista de Paises
            </Typography>
            <TableList data={getCountries()} headers={headers} keys={keys} actions={actions} primaryKey={primaryKey} />
            </Paper>
                    <Grid container item xs={12} className="FormFooter" spacing={Number(spacing)}>
                        <Button className={classes.button} href="addCountry" variant='contained' color='primary'>Agregar País</Button>
                    </Grid>
            </Grid>      
          
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedViewCountryPage = withStyles(styles)(connect(mapStateToProps)(ViewCountryPage));
export { connectedViewCountryPage as ViewCountryPage };