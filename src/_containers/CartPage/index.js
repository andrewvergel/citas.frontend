import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import '../../_css/App.css';
import {    getMonths, 
            getYears, 
            getBanks    } from '../../_helpers/fake-data';


const TAX_RATE = 0.16;

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  button: {
    marginRight : theme.spacing.unit,
  },
  table: {
    minWidth: 700,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
});


function ccyFormat(num) {
    return `${num.toFixed(2)}`;
  }
  
  function priceRow(qty, unit) {
    return qty * unit;
  }
  
  function createRow(id, desc, qty, unit) {
    const price = priceRow(qty, unit);
    return { id, desc, qty, unit, price };
  }
  
  function subtotal(items) {
    return items.map(({ price }) => price).reduce((sum, i) => sum + i, 0);
  }
  
  const rows = [
    ['Manicure', 1, 2534.00],
  ].map((row, id) => createRow(id, ...row));
  
  const invoiceSubtotal = subtotal(rows);
  const invoiceTaxes = TAX_RATE * invoiceSubtotal;
  const invoiceTotal = invoiceTaxes + invoiceSubtotal;
  const typeDocuments = [
    {
      value: 'C',
      label: 'C.I.',
    },
    {
      value: 'P',
      label: 'Pasaporte',
    },
    {
      value: 'D',
      label: 'DNI',
    },
  ];
  const expirationMonths = getMonths();
  const expirationYears = getYears();
  const banks = getBanks();

class CartPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          //pickedData : getPickedData(),
          methodOfPayment : '',
          open: false,
          openTrans: false,
          scroll: 'paper',
          spacing: '16',
        };

        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClickOpenTrans = this.handleClickOpenTrans.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleCloseTrans = this.handleCloseTrans.bind(this);
      }

    handleClickOpen() {
    this.setState({ open: true});
    };

    handleClickOpenTrans() {
        this.setState({ openTrans: true});
        };

    handleClose() {
    this.setState({ open: false });
    };
    
    handleCloseTrans() {
        this.setState({ openTrans: false });
    };

    render() {
        const { user, users, classes  } = this.props;
        const { spacing } = this.state;
        return (
            <Grid item xs={12} sm={8} className="ContentApp">
            <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                        <TableRow>
                            <TableCell>Servicio</TableCell>
                            <TableCell numeric>Cantidad.</TableCell>
                            <TableCell numeric>Precio Unitario</TableCell>
                            <TableCell numeric>Costo</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {rows.map(row => {
                            return (
                            <TableRow key={row.id}>
                                <TableCell>{row.desc}</TableCell>
                                <TableCell numeric>{row.qty}</TableCell>
                                <TableCell numeric>{row.unit}</TableCell>
                                <TableCell numeric>{ccyFormat(row.price)}</TableCell>
                            </TableRow>
                            );
                        })}
                        <TableRow>
                            <TableCell rowSpan={3} />
                            <TableCell colSpan={2}>Subtotal</TableCell>
                            <TableCell numeric>{ccyFormat(invoiceSubtotal)}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>Impuesto</TableCell>
                            <TableCell numeric>{`${(TAX_RATE * 100).toFixed(0)} %`}</TableCell>
                            <TableCell numeric>{ccyFormat(invoiceTaxes)}</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell colSpan={2}>Total</TableCell>
                            <TableCell numeric>{ccyFormat(invoiceTotal)}</TableCell>
                        </TableRow>
                        </TableBody>
                    </Table>
                    
            </Paper>
                    <Grid container item xs={12} className="FormFooter" spacing={Number(spacing)}>
                        <Button className={classes.button} onClick={this.handleClickOpen} variant='contained' color='primary'>Pagar con TDC</Button>
                        <Dialog
                        open={this.state.open}
                        onClose={this.handleClose}
                        aria-labelledby="form-dialog-title"
                        >
                        <DialogTitle id="form-dialog-title">Pagar cita</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                            Por favor coloque los datos de su TDC para proceder a confirmar su cita.
                            </DialogContentText>
                            <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Nombre y Apellido del Titular"
                            fullWidth
                            />
                            <TextField
                                id="standard-select-type-document-native"
                                select
                                label=""
                                className={classes.textField}
                                value={this.state.typedocument}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                    className: classes.menu,
                                    },
                                }}
                                helperText="Tipo de documento"
                                margin="normal"
                                >
                                {typeDocuments.map(option => (
                                    <option key={option.value} value={option.value}>
                                    {option.label}
                                    </option>
                                ))}
                                </TextField>
                                <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Documento"
                                />
                                <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Número de Tarjeta de Crédito"
                                fullWidth
                            />
                            <TextField
                                id="standard-select-expiration-month-native"
                                select
                                label=""
                                className={classes.textField}
                                value={this.state.expirationMonth}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                    className: classes.menu,
                                    },
                                }}
                                helperText="Mes de Vencimiento"
                                margin="normal"
                                >
                                {expirationMonths.map(option => (
                                    <option key={option.value} value={option.value}>
                                    {option.label}
                                    </option>
                                ))}
                                </TextField>
                                <TextField
                                id="standard-select-expiration-year-native"
                                select
                                label=""
                                className={classes.textField}
                                value={this.state.expirationMonth}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                    className: classes.menu,
                                    },
                                }}
                                helperText="Año de Vencimiento"
                                margin="normal"
                                >
                                {expirationYears.map(option => (
                                    <option key={option.value} value={option.value}>
                                    {option.label}
                                    </option>
                                ))}
                                </TextField>
                                <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="CVV"
                                />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                            Pagar
                            </Button>
                            <Button onClick={this.handleClose} color="secondary">
                            Cancelar
                            </Button>
                        </DialogActions>
                        </Dialog>
                        <Button className={classes.button} onClick={this.handleClickOpenTrans} variant='contained' color='secondary'>Pagar con transferencia</Button>
                        <Dialog
                        open={this.state.openTrans}
                        onClose={this.handleCloseTrans}
                        aria-labelledby="form-dialog-title"
                        >
                        <DialogTitle id="form-dialog-title">Pagar cita</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                            Por favor coloque los datos de la transferencia para proceder a confirmar su cita.
                            </DialogContentText>
                            <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Número de Transferencia"
                            fullWidth
                            />
                            <TextField
                                id="standard-select-type-document-native"
                                select
                                label=""
                                className={classes.textField}
                                value={this.state.typedocument}
                                SelectProps={{
                                    native: true,
                                    MenuProps: {
                                    className: classes.menu,
                                    },
                                }}
                                helperText="Banco"
                                margin="normal"
                                >
                                {banks.map(option => (
                                    <option key={option.value} value={option.value}>
                                    {option.label}
                                    </option>
                                ))}
                                </TextField>
                                <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                label="Fecha de Transferencia"
                                />
                        </DialogContent>
                        <DialogActions>
                            <Button component={Link} to="/" color="primary">
                            Guardar
                            </Button>
                            <Button onClick={this.handleCloseTrans} color="secondary">
                            Cancelar
                            </Button>
                        </DialogActions>
                        </Dialog>
                    </Grid>
            </Grid>      
          
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedCartPage = withStyles(styles)(connect(mapStateToProps)(CartPage));
export { connectedCartPage as CartPage };