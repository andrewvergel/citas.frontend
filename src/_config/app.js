const AppConfig = {
    header : {
        drawerWidth: 260
    },
    theme : {
        typography: {
            useNextVariants: true,
        },
        palette: {
          primary: {
              main:'#5f3d08',
          },
          secondary: { 
              main: '#e62783' 
          },
          text:{
              primary: '#5f3d08',
              secondary: '#6d6d6d'
          }
        },
    },
    footer : {
        links: [
            {
                href: 'https://spamanos.com/',
                title: 'Spa Manos',
            },
            {
                href: 'https://spamanosvenezuela.com/',
                title: 'Spa Manos - Venezuela',
            }
        ]
    }
}

export default AppConfig;

