import { userConstants } from '../_constants/constants';
import  UserService from '../_services/User.service';

class UserActions {

    static getAll() {
        return dispatch => {
            dispatch(request());

            UserService.getAll()
                .then(
                    users => dispatch(success(users)),
                    error => dispatch(failure(error.toString()))
                );
        };

        function request() { return { type: userConstants.GETALL_REQUEST } }
        function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
        function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
    }

    // prefixed function name with underscore because delete is a reserved word in javascript
    static _delete(id) {
        return dispatch => {
            dispatch(request(id));

            UserService.delete(id)
                .then(
                    user => dispatch(success(id)),
                    error => dispatch(failure(id, error.toString()))
                );
        };

        function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
        function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
        function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
    }

}

export default UserActions;