import { countryConstants } from '../_constants/constants';
import AlertActions from './Alert.actions';
import CountryService  from '../_services/Country.service';
import { history } from '../../_core/_helpers/history';

class CountryActions {

    static getById( id ) {

        return dispatch => {
            dispatch(request());

            CountryService.getById(id)
            .then(
                countries => dispatch(success(countries)),
                error => dispatch(failure(error.toString()))
            );
        };

        function request() { return { type: countryConstants.GET_REQUEST } }
        function success(countries) { return { type: countryConstants.GET_SUCCESS, countries } }
        function failure(error) { return { type: countryConstants.GET_FAILURE, error } }
    }

    static getAll( params ) {

        return dispatch => {
            dispatch(request());

            CountryService.getAll(params)
            .then(
                countries => dispatch(success(countries)),
                error => dispatch(failure(error.toString()))
            );
        };

        function request() { return { type: countryConstants.GET_REQUEST } }
        function success(countries) { return { type: countryConstants.GET_SUCCESS, countries } }
        function failure(error) { return { type: countryConstants.GET_FAILURE, error } }
    }

    static add(country){

        console.log('country',country);

        return dispatch => {
            
            dispatch(AlertActions.clear());
            dispatch(request());
    
            CountryService.add(country)
            .then(
                country => { 
                    dispatch(success(country));
                    history.push('/countries');
                    dispatch(AlertActions.success('Pais agregado con exito.'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(AlertActions.error(error.toString()));
                }
            );
        };

        function request() { return { type: countryConstants.ADD_REQUEST } }
        function success(country) { return { type: countryConstants.ADD_SUCCESS, country } }
        function failure(error) { return { type: countryConstants.ADD_FAILURE, error } }

    }

    static edit(country){

        console.log('country',country);

        return dispatch => {
            
            dispatch(AlertActions.clear());
            dispatch(request());
    
            CountryService.edit(country)
            .then(
                country => { 
                    dispatch(success(country));
                    dispatch(AlertActions.success('Pais actualizado con exito.'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(AlertActions.error(error.toString()));
                }
            );
        };

        function request() { return { type: countryConstants.PUT_REQUEST } }
        function success(country) { return { type: countryConstants.PUT_SUCCESS, country } }
        function failure(error) { return { type: countryConstants.PUT_FAILURE, error } }

    }

    static delete(id){

  
        return dispatch => {
            
            dispatch(AlertActions.clear());
            dispatch(request());
    
            CountryService.delete(id)
            .then(
                countries => { 
                    dispatch(success());
                    dispatch(AlertActions.success('País eliminado con exito.'));
                    dispatch(CountryActions.getAll())
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(AlertActions.error(error.toString()));
                }
            );
        };

        function request() { return { type: countryConstants.DELETE_REQUEST } }
        function success() { return { type: countryConstants.DELETE_SUCCESS } }
        function failure(error) { return { type: countryConstants.DELETE_FAILURE, error } }

    }

} 

export default CountryActions;