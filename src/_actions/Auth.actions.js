import { userConstants } from '../_constants/constants';
import AlertActions from './Alert.actions';
import AuthService  from '../_services/Auth.service';
import { history } from '../../_core/_helpers/history';

class AuthActions {

    static logout(){
        AuthService.logout();
        return { type: userConstants.LOGOUT };
    }

    static login({username, password} = {}) {
        
        return dispatch => {
            
            dispatch(AlertActions.clear());
            dispatch(request());
    
            AuthService.login({username, password})
                .then(
                    user => { 
                        dispatch(success(user));
                        history.push('/');
                    },
                    error => {
                        dispatch(failure(error.toString()));
                        dispatch(AlertActions.error(error.toString()));
                    }
                );
        };
    
        function request() { return { type: userConstants.LOGIN_REQUEST } }
        function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
        function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
    }

    static register(user){

        return dispatch => {
            
            dispatch(request());
    
            AuthService.register(user)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/login');
                    dispatch(AlertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(AlertActions.error(error.toString()));
                }
            );
        };

        function request() { return { type: userConstants.LOGIN_REQUEST } }
        function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
        function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }

    }

} 

export default AuthActions;