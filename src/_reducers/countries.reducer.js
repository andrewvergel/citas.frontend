import { countryConstants } from '../_constants/constants';

export function countries(state = {}, action) {
  console.log("**** Action :: ",action);
  switch (action.type) {
    case countryConstants.GET_REQUEST:
      return {
        loading: true
      };
    case countryConstants.GET_SUCCESS:
      return {
        data: action.countries
      };
    case countryConstants.GET_FAILURE:
      return { 
        error: action.error
      };
    case countryConstants.ADD_REQUEST:
      return {
        loading: true
      };
    case countryConstants.ADD_SUCCESS:
      return {
        data: action.countries
      };
    case countryConstants.ADD_FAILURE:
      return { 
        error: action.error
      };
    case countryConstants.PUT_REQUEST:
      return {
        loading: true
      };
    case countryConstants.PUT_SUCCESS:
      return {
        data: action.countries
      };
    case countryConstants.PUT_FAILURE:
      return { 
        error: action.error
      };
    case countryConstants.DELETE_REQUEST:
      return {
        loading: true
      };
    case countryConstants.DELETE_SUCCESS:
      return {
        data: action.countries
      };
    case countryConstants.DELETE_FAILURE:
      return { 
        error: action.error
      };
    default:
      return state
  }
}