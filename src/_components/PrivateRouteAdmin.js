import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthService  from '../_services/Auth.service'

export const PrivateRouteAdmin = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        ( AuthService.getUserProfile() == 1 )
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)