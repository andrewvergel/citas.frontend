import React from 'react';
import { connect } from 'react-redux';
import empty from 'is-empty'

import TableList from '../../../_core/_ui/_components/TableList';
import TableHeader from '../../../_core/_ui/_components/TableHeader';
import Loader from '../../../_core/_ui/_components/Loader';

import { withStyles } from '@material-ui/core/styles';

import CountryActions from '../../_actions/Country.actions';
import ApiService from '../../../_core/_backend/_services/Api.service';

import AddIcon from '@material-ui/icons/AddCircle';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/RemoveCircle';

const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    button: {
      marginRight : theme.spacing.unit,
    },
    table: {
      minWidth: 700,
    },
    control: {
      padding: theme.spacing.unit * 2,
    },
  });

class CountriesList extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
          params: ApiService.getDefaultParams()
        }
        
    }

    componentWillReceiveProps(nextProps){

      let { countries } = nextProps;

      if (!empty(countries.data)) {
          this.setState({
            countries
          })
      }

  }

    componentDidMount(){
      let { params } = this.state;
      this.props.dispatch(CountryActions.getAll(params));
    }

    deleteCountry(element, params){
      params.props.dispatch(CountryActions.delete(element.id));
    }

    render() {
    
        const { countries } = this.props;

        console.log("CountriesList:this.props", this.props); 
        console.log("CountriesList:countries ",countries);

        let headers = ['Id','País']; // send headers
        let keys = ['id','name']; // send key
        let primaryKey = 'id';
        let actions = {
            'id' :  {
              url : {
                slug : "/country/view/$1",
                params: [ 'id' ]
              }
            },
            'Editar' :  {
              button : {
                icon: <EditIcon/>,
              },
              url : {
                slug : "/country/edit/$1",
                params: [ 'id' ]
              }
            },
            'Eliminar' : {
              button : {
                icon: <DeleteIcon/>,
              },
              url : {
                slug : "/country/delete/$1",
                params: [ 'id' ]
              },
              cb : {
                f : this.deleteCountry,
                params : {
                  props :this.props
                }
              }
            },
        };

        const listHeaderActions = [
          {
            type: 'button',
            icon : <AddIcon/>,
            label : 'Nuevo',
            cb : () => {
              this.props.history.push(`/country/add`)
            }
          }
        ]

        const data = !empty(countries.data) ? countries.data : {};
        
        return (
            <div className="List">

              <Loader loading={(countries.loading)} />
              
                <TableHeader  {...this.props} 
                              actions={listHeaderActions}
                              title='Listado de Paises' />

                <TableList  {...this.props}
                            data={data} 
                            headers={headers} 
                            keys={keys} 
                            actions={actions} 
                            primaryKey={primaryKey} /> 

            </div>

        );
    }
}

function mapStateToProps(state) {
  const { users, authentication, countries } = state;
  const { user } = authentication;
  return {
      user,
      users,
      countries
  };
}

const connectedCountriesList = withStyles(styles)(connect(mapStateToProps)(CountriesList));
export { connectedCountriesList as CountriesList };