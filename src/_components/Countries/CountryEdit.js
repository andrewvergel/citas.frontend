/**
 * Formulario de agregar pais
 * autor: Andres Rosales
 * fecha: 09/01/2019
 */

import React from 'react';
import { connect } from 'react-redux';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import empty from 'is-empty'
import Validator from 'validator'
import FormsUtils from '../../../_core/_ui/_utils/FormsUtils'
import Loader from '../../../_core/_ui/_components/Loader';

import CountryActions from '../../_actions/Country.actions';

class CountryEdit extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            model: {
                id: null,
                name: '',
            },
            touched: {
                name: false,
            },
            activedButtonSubmit: false,
            errors: {},
            submitted: false
        };

      }

      componentDidMount(){

        console.log(this.props);
        let { match } =  this.props;

        if (!empty(match.params.id) ) {
            this.props.dispatch(CountryActions.getById(match.params.id));
        }

      }

      validate(){
        
        const   {   model,
                    touched }   = this.state;
        let     {    name   }   = model;
        let     errors  = {}
        
        if(touched.name) {
            if(Validator.isEmpty(name)) errors.name = 'Nombre de Pais es un campo obligatorio'
        }
        
        this.setState({
            errors
        }, () => {
            FormsUtils.shouldBeSubmit(this)
        })

        return (empty(errors)) ? true : false;
    }

    handleSubmit(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        const { model } = this.state;
        this.setState({ submitted: true });
        dispatch(CountryActions.edit(model));
    }

    componentWillReceiveProps(nextProps){

        console.log("CountryEdit componentWillReceiveProps::nextProps",nextProps);

        let { countries } = nextProps;

        if (!empty(countries.data)) {

            this.setState({
                model: {
                    id:     !empty(countries.data.id) ? countries.data.id : null,
                    name:   !empty(countries.data.name) ? countries.data.name : ''
                },
                touched : {
                    name : !empty(countries.data.name) ? true : false
                },
                activedButtonSubmit: true
            })

        }

    }

    render() {

        const   {   model,
                    errors, 
                    activedButtonSubmit } = this.state;

        const { countries } = this.props;
        
        return (
          <div>

            <Loader loading={(countries.loading && empty(countries.data))} />

            <Typography variant="h5" 
                        component="h3" 
                        color="textPrimary" 
                        className="FormHeader" >  
                Editar País
            </Typography>

            <form   name="form" 
                    onSubmit={(e) => this.handleSubmit(e)} >

                <Grid container spacing={24}>

                    <Grid   item 
                            xs={6} >

                            <TextField
                                    {...((errors.name) && { error: true })}
                                    helperText={(errors.name) ? errors.name : '' } 
                                    id="Countryname"
                                    label="Nombre del Pais"
                                    name='name'
                                    className="FormInputText"
                                    value={model.name}
                                    onChange={(e) => FormsUtils.handleChange({e, context: this, target: 'model' })}
                                    onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                    margin="normal"
                            />
                    </Grid>

                    <Grid   item 
                            xs={12} 
                            className="FormFooter">

                        <Button className="FormButton" 
                                type="submit" 
                                variant="contained" 
                                color="primary" 
                                disabled={!activedButtonSubmit} >
                                Actualizar
                        </Button>

                        <Button className="FormButton" 
                                type="button" 
                                variant="contained" 
                                color="secondary" 
                                onClick={() => { this.props.history.push(`/countries`) }} >
                                Volver
                        </Button>

                    </Grid>

                </Grid>

            </form>

          </div>
        );
    }
}

function mapStateToProps(state) {
  const { authentication, countries } = state;
  const { user } = authentication;
  return {
      user,
      countries
  };
}

export default connect(mapStateToProps)(CountryEdit);