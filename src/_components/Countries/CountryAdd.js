/**
 * Formulario de agregar pais
 * autor: Andres Rosales
 * fecha: 09/01/2019
 */

import React from 'react';
import { connect } from 'react-redux';

import CountryActions from '../../_actions/Country.actions';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import empty from 'is-empty'
import Validator from 'validator'
import FormsUtils from '../../../_core/_ui/_utils/FormsUtils'
import Loader from '../../../_core/_ui/_components/Loader';

class CountryAdd extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: null,
            model: {
                name: '',
            },
            touched: {
                name: false,
            },
            activedButtonSubmit: false,
            errors: {},
            submitted: false
        };

      }

      validate(){
        
        const   {   model,
                    touched }   = this.state;
        let     {    name   }   = model;
        let     errors  = {}
        
        if(touched.name) {
            if(Validator.isEmpty(name)) errors.name = 'Nombre de Pais es un campo obligatorio'
        }
        
        this.setState({
            errors
        }, () => {
            FormsUtils.shouldBeSubmit(this)
        })

        return (empty(errors)) ? true : false;
    }

    handleSubmit(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        const { model } = this.state;
        this.setState({ loading:true, submitted: true });
        dispatch(CountryActions.add(model));
    }

    render() {

        const   {   model,
                    loading,
                    errors, 
                    activedButtonSubmit } = this.state;
        let     {   name } = model;

        return (

          <div>

            <Loader loading={loading} />

            <Typography variant="h5" 
                        component="h3" 
                        color="textPrimary" 
                        className="FormHeader" >  
                Agregar País
            </Typography>

            <form   name="form" 
                    onSubmit={(e) => this.handleSubmit(e)} >

                <Grid container spacing={24}>

                    <Grid   item 
                            xs={6} >

                            <TextField
                                    {...((errors.name) && { error: true })}
                                    helperText={(errors.name) ? errors.name : '' } 
                                    id="Countryname"
                                    label="Nombre del Pais"
                                    name='name'
                                    className="FormInputText"
                                    value={name}
                                    onChange={(e) => FormsUtils.handleChange({e, context: this, target: 'model' })}
                                    onBlur={(e) => FormsUtils.handleBlur(e,this)}
                                    margin="normal"
                            />
                    </Grid>

                    <Grid   item 
                            xs={12} 
                            className="FormFooter">

                        <Button className="FormButton" 
                                type="submit" 
                                variant="contained" 
                                color="primary" 
                                disabled={!activedButtonSubmit} >
                                Guardar
                        </Button>

                        <Button className="FormButton" 
                                type="button" 
                                variant="contained" 
                                color="secondary" 
                                onClick={() => { this.props.history.push(`/countries`) }} >
                                Cancelar
                        </Button>

                    </Grid>
                
                </Grid>

            </form>

          </div>
        );
    }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
      user
  };
}

export default connect(mapStateToProps)(CountryAdd);