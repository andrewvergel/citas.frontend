import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

class ContentApp extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const TagName = this.props.component;
    return (
      <Grid item xs={12} sm={12} className="contentApp">
        <Paper className="contentComponent">
           <TagName {...this.props}/>
        </Paper>
      </Grid> 
    );

  }
  
}

export default ContentApp;