import React from 'react';
import { connect } from 'react-redux';
import empty from 'is-empty'
import AppConfig from '../_config/app'

import { withStyles } from '@material-ui/core/styles';

import PersonIcon from '@material-ui/icons/Person';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import DashboardIcon from '@material-ui/icons/Dashboard';
import CountryIcon from '@material-ui/icons/LocationCity'
import HomeIcon from '@material-ui/icons/Home';
import ExitToAppIcon from '@material-ui/icons/ExitToApp'

import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

const drawerWidth = AppConfig.header.drawerWidth;

const styles = theme => ({
  list:{
    width: drawerWidth,
  },
  appBar: {
    marginLeft: drawerWidth,
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  }
});

class Menu extends React.Component {

  render() {

      console.log("props", this.props);

      let { open, theme, user, classes } = this.props;
      let menuLinks = [];
      let menu = [];

      menu[0] = [
        {
          title: 'Inicio de Sesion',
          target: '/login',
          icon: <PersonIcon />
        },
        {
          title: 'Registro',
          target: '/register',
          icon: <PersonAddIcon />
        }
      ]

      menu[1] = [
        {
          title: 'Home',
          target: '/',
          icon: <HomeIcon />
        },
        {
          title: 'Dashboard',
          target: '/dashboard',
          icon: <DashboardIcon />
        },
        ,
        {
          title: 'Paises',
          target: '/countries',
          icon: <CountryIcon />
        },
        {
          title: 'Salir',
          target: '/login',
          icon: <ExitToAppIcon />
        }
      ];

      menuLinks = menu[0] ; 

      if ( ! empty( user ) ) {
         
        if( !empty(user.type )){

            if (!empty(menu[user.type])) {
              menuLinks = menu[user.type]
            }
            
        }

      }

      const items = (
        <div className={classes.list}>
        <List>
            { 
              menuLinks.map((item, index) => (

                <ListItem button 
                          key={index} 
                          onClick={() => { this.props.history.push(item.target) } } >

                  <ListItemIcon>
                    { item.icon }
                  </ListItemIcon>

                  <ListItemText primary={item.title} />

                </ListItem>


              ))
            }
        </List>
        </div>

      );

      return (
        <nav>

        <Hidden smUp 
                implementation="css">

          <Drawer container={this.props.container}
                  variant="temporary"
                  open={open}
                  onClose={this.props.handleDrawerToggle}
          >

              {items}

          </Drawer>
        </Hidden>

        <Hidden xsDown 
                implementation="css">
            <Drawer
              variant="permanent"
              open
            >
                  {items}

            </Drawer>
        </Hidden>


      </nav>
      );
  }
  
}


function mapStateToProps(state) {
  const { users, authentication } = state;
  const { user } = authentication;
  return {
      user,
      users
  };
}

const MenuComponent = withStyles(styles)(connect(mapStateToProps)(Menu));
export default MenuComponent;