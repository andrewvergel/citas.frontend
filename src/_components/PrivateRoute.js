import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthService  from '../_services/Auth.service'
import empty from 'is-empty'

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        ( !empty(AuthService.getUserProfile()) )
            ? <Component {...props} />
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)