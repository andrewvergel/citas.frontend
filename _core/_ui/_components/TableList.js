import React from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import empty from 'is-empty'
import ListUtils from '../_utils/ListUtils'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const styles = theme => ({
    table: {
      minWidth: 700,
    }
  });

class TableList extends React.Component {

    constructor(props) {
        super(props);
    }

    buildHeaders()
    {
        let rowsHeader=[],rowHeaderAction=[],
        headers = this.props.headers,
        actions = this.props.actions;

        for (var action in actions)
        {
            if( !empty(actions)  ) {
                rowHeaderAction.push(<TableCell key='actions'>Acciones</TableCell>);
                break;
            }
        }

        for (var header in headers) {

            rowsHeader.push(<TableCell key={header}>{headers[header]}</TableCell>);
        }

        return (<TableHead><TableRow key='row_header'>{rowsHeader}{rowHeaderAction}</TableRow></TableHead>);
    }

    buildBody() {

        console.log("*** TableList::props ",this.props)

        let rowsBody    =   [],
            { data, actions, primaryKey, keys } =   this.props;

            if(!empty(data.data)) {

                let records = data.data;

                records.forEach(

                    function(element) {

                        let cellsBody       = [],
                            actionLinks     = [],
                            primaryValue    = element[primaryKey];

                        for (var key in keys) { 

                            if( keys[key] == primaryKey ) {

                                if (!empty(actions['id'])) {

                                    let linkTo = ListUtils.getUrlByAction({
                                        id : 'id',
                                        element,
                                        actions
                                    });

                                    let label = element[keys[key]];

                                    cellsBody.push(<TableCell key={element[keys[key]]}>
                                                        <Link   to={linkTo} 
                                                                className="list-action-link">
                                                            {label}
                                                        </Link>
                                                    </TableCell>);

                                }
                                

                            } else {
                                cellsBody.push(<TableCell key={element[keys[key]]}>{element[keys[key]]}</TableCell>);
                            }
                        }

                        if (!empty(actions)) {

                            for (let action in actions) {

                                if (action != 'id') {

                                    let linkTo = ListUtils.getUrlByAction({
                                        id : action,
                                        element,
                                        actions
                                    });

                                    let onClick = () => {};
                                    if (!empty(actions[action].cb )){
                                        onClick = (e) => { 
                                            e.preventDefault();
                                            actions[action].cb.f(element,actions[action].cb.params) 
                                        }
                                    }

                                    let labelLink = action;

                                    if(!empty(actions[action].button)){
                                        
                                        let { icon, label } = actions[action].button;              
                                        
                                        if (!empty(icon)){
                                            labelLink = icon;
                                        } 

                                    }
                                    
                                    actionLinks.push(  <Link    key={ action + '_'+ primaryValue } 
                                                                to={ linkTo } 
                                                                onClick={ onClick }
                                                                className="list-action-link">
                                                           { labelLink }
                                                        </Link> )

                                }
                                
                            }

                        }

                        cellsBody.push(<TableCell key={'action_'.concat(primaryValue)} >{actionLinks} </TableCell>);

                        rowsBody.push(<TableRow key={'row_'.concat(primaryValue)}>{cellsBody}</TableRow>);
                    }
                

                )
            }
            
            return (<TableBody>{rowsBody}</TableBody>);
    }

    render() {
        return (
        <Table className="list">
            {this.buildHeaders()}
            {this.buildBody()}
        </Table>
        );
    }
}

export default TableList;