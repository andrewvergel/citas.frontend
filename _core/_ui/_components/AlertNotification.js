import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class AlertNotification extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount(){
        console.log("Trigger:ALERT",this.props);

        const { message, type } = this.props;
        const options = {
            hideProgressBar: true,
            pauseOnHover: true,
            position: toast.POSITION.TOP_CENTER
        };

        if (type == 'success'){
            toast.success(message, options );
        }

        if (type == 'error'){
            toast.error(message, options);
        }
    }

    render() {
        return (
            <div>
                <ToastContainer />
            </div>
        );
    }

}

export default AlertNotification;