import React from 'react';
import empty from 'is-empty'
import ConfigApp from '../../../src/_config/app'
import ClipLoader from 'react-spinners/ClipLoader';

const override = `
    display: block;
    margin: 0 auto;
    border-color: red;
`;


class Loader extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { loading } =  this.props;

        if ( !empty(loading) ) {
            return (
                <div className="overlay">
                        <div className="contentLoading">
                        <ClipLoader
                                css={override}
                                sizeUnit={"px"}
                                size={40}
                                color={ConfigApp.theme.palette.primary.main}
                        />
                        </div>
                </div>
            );
        } else{
            return ''
        }

        
    }

}

export default Loader;