import empty from 'is-empty'

class ListUtils {

    static getUrlByAction({ id, actions, element } = {} ) {

        let action =  actions[id];
        let returnSlug = ''
        let baseSlug = action.url.slug;
        let params = action.url.params;

        returnSlug = baseSlug.replace(/\$[0-9]{1,2}/gi, function(matched){
            let findId = parseInt(matched.replace('$','')) - 1 ;
            return (!empty(element[params[findId]])) ? element[params[findId]] : '' ;
        });

        return returnSlug;

    }

}


export default ListUtils;