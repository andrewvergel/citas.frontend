import empty from 'is-empty'

class Utils {

    static getComponentByUrlList({ urls, uri } = {} ) {

        let element = '';

        for(let url in urls ) {

            if(uri.match(url)) {
                // console.log('url',url);
                // console.log('element',urls[url]);
                element = urls[url];
                break
            }
        } 

        if (empty(element)){
            console.log(`*** no se encontro componente para esta url`)
        }

        return (!empty(element)) ? element : '';
    }

}


export default Utils;